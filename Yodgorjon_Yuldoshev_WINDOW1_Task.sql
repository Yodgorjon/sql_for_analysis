--Retrieve customers who ranked among the top 300 in sales for the years 1998, 1999, and 2001.
--Categorize the customers based on their sales channels.
--Perform separate calculations for each sales channel.
--Include in the report only purchases made on the channel specified.
--Format the column so that total sales are displayed with two decimal places.
WITH ranked_customers AS (
    SELECT
        s.cust_id,
        c.channel_desc,
        ROUND(SUM(s.amount_sold)::numeric, 2) AS total_sales,
        RANK() OVER (PARTITION BY s.channel_id, EXTRACT(YEAR FROM s.time_id) ORDER BY SUM(s.amount_sold) DESC) AS sales_rank
    FROM
        sh.sales s
    INNER JOIN
        sh.channels c ON s.channel_id = c.channel_id
    WHERE
        EXTRACT(YEAR FROM s.time_id) IN (1998, 1999, 2001)
    GROUP BY
        s.cust_id, s.channel_id, c.channel_desc, EXTRACT(YEAR FROM s.time_id)
)
SELECT
    cust_id,
    channel_desc,
    total_sales
FROM
    ranked_customers
WHERE
    sales_rank <= 300
ORDER BY
    channel_desc, total_sales DESC;
