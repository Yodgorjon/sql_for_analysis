--Retrieve the total sales amount for each product category for a specific time period
SELECT p.prod_category, SUM(s.amount_sold) AS total_sales_amount
FROM sh.sales s
JOIN sh.products p ON s.prod_id = p.prod_id
WHERE s.time_id BETWEEN 'start_date' AND 'end_date'  -- Specify your time period
GROUP BY p.prod_category;

--Calculate the average sales quantity by region for a particular product
SELECT r.country_region, AVG(s.quantity_sold) AS avg_sales_quantity
FROM sh.sales s
JOIN sh.customers c ON s.cust_id = c.cust_id
JOIN sh.countries r ON c.country_id = r.country_id
WHERE s.prod_id = 'your_product_id'  -- Specify your product ID
GROUP BY r.country_region;

--Find the top five customers with the highest total sales amount
SELECT c.cust_id, c.cust_first_name, c.cust_last_name, SUM(s.amount_sold) AS total_sales_amount
FROM sh.sales s
JOIN sh.customers c ON s.cust_id = c.cust_id
GROUP BY c.cust_id, c.cust_first_name, c.cust_last_name
ORDER BY total_sales_amount DESC
LIMIT 5;
