--Determine the sales for each subcategory from 1998 to 2001.
--Calculate the sales for the previous year for each subcategory.
--Identify subcategories where the sales from 1998 to 2001 are consistently higher than the previous year.
--Generate a dataset with a single column containing the identified prod_subcategory values.
WITH subcategory_sales AS (
    SELECT
        prod_subcategory,
        EXTRACT(YEAR FROM s.time_id) AS sales_year,
        SUM(s.amount_sold) AS total_sales,
        LAG(SUM(s.amount_sold)) OVER (PARTITION BY prod_subcategory ORDER BY EXTRACT(YEAR FROM s.time_id)) AS prev_year_sales
    FROM
        sh.sales s
    INNER JOIN
        sh.products p ON s.prod_id = p.prod_id
    WHERE
        EXTRACT(YEAR FROM s.time_id) BETWEEN 1998 AND 2001
    GROUP BY
        prod_subcategory, sales_year
)
SELECT DISTINCT
    prod_subcategory
FROM
    subcategory_sales
WHERE
    total_sales > COALESCE(prev_year_sales, 0)
ORDER BY
    prod_subcategory;
