WITH week_sales AS (
    SELECT
        time_id,
        amount_sold,
        SUM(amount_sold) OVER (
            PARTITION BY EXTRACT(WEEK FROM time_id)  -- Partition by week
            ORDER BY time_id
        ) AS cum_sum,
        CASE
            WHEN EXTRACT(DOW FROM time_id) = 1 THEN
                AVG(amount_sold) OVER (
                    PARTITION BY EXTRACT(WEEK FROM time_id)  -- Partition by week
                    ORDER BY time_id
                    ROWS BETWEEN 3 PRECEDING AND 1 FOLLOWING  -- Monday: Avg of 4 days incl. weekend
                )
            WHEN EXTRACT(DOW FROM time_id) = 5 THEN
                AVG(amount_sold) OVER (
                    PARTITION BY EXTRACT(WEEK FROM time_id)  -- Partition by week
                    ORDER BY time_id
                    ROWS BETWEEN 1 PRECEDING AND 2 FOLLOWING  -- Friday: Avg of 3 days incl. weekend
                )
            ELSE
                AVG(amount_sold) OVER (
                    PARTITION BY EXTRACT(WEEK FROM time_id)  -- Partition by week
                    ORDER BY time_id
                    ROWS BETWEEN 1 PRECEDING AND 1 FOLLOWING  -- Other days: Avg of 3 consecutive days
                )
        END AS centered_3_day_avg
    FROM
        sh.sales
    WHERE
        -- Filter for weeks 49, 50, and 51 of 1999
        EXTRACT(WEEK FROM time_id) BETWEEN 49 AND 51
        AND EXTRACT(YEAR FROM time_id) = 1999
)
SELECT
    time_id,
    amount_sold,
    cum_sum,
    centered_3_day_avg
FROM
    week_sales
ORDER BY
    time_id;
